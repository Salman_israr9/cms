<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Medicine;
use App\Symptoms;
class MedicineController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function medicineList()
    {
        return view('medicine.medicineList');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addMedicineForm()
    {
        return view('medicine.addMedicine');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function saveMedicine(Request $request)
    {
        Medicine::create($request->all());
        return redirect(url('medicine-list'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editMedicineForm($id)
    {
        $medicine =Medicine::findorfail($id);

        return view("medicine.editMedicine" ,compact('medicine'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateMedicine(Request $request, $id)
    {
        
        Medicine::findorfail($id)->update($request->all());
        return redirect(url('medicine-list'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteMedicine($id)
    {
        Medicine::findorfail($id)->delete();
        return redirect(url('medicine-list'));
    }


    // symptoms methods 

    public function symptomList()
    {
        return view('symptoms.symptomList');
    }
    
    public function addSymptomForm()
    {
        return view('symptoms.addSymptom');
    }

    public function saveSymptom( Request $request)
    {

       $s = new Symptoms();
       $s->title = $request->title;
       $s->value = $request->value;
       $s->save();
        return redirect(url('symptoms-list'));
    }


    public function editSymptomForm($id)
    {
        $symptom = Symptoms::findorfail($id);
        return view('symptoms.editSymptom',compact('symptom'));
    }
    

    public function updateSymptom(Request $request,$id)
    {

        $symptom = Symptoms::findorfail($id);
        $symptom->update($request->all());
        return redirect(url('symptoms-list'));
    }

     public function deleteSymptom($id)
    {
        $symptom = Symptoms::findorfail($id);
        $symptom->delete();
        return redirect(url('symptoms-list'));
    }
    
        

    
}
