<?php

namespace App\Http\Controllers;
use App\Patient;
use Illuminate\Http\Request;
use App\Appointment;
use App\Symptoms;
use App\PatientMedicine;
use App\Test;
use App\Diagnosis;
use App\Income;
use App\Expenses;
use App\Diet;

class PatientController extends Controller
{
    /**
     * Didietlay a listing of the resource.
     *
     * @return \Illuminate\Http\Redietonse
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function patientList()
    {
        return view('Patient.patientList');
    }

    public function dashboard()
    {
        return view('layouts.overview');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Redietonse
     */
    public function addPatient()
    {

        return view('patient.addPatient');        
    }

    public function backup()
    {
        //return view('Patient.backup');
        define("BACKUP_PATH", public_path()."\\");

        $server_name   = "localhost";
        $username      = "root";
        $password      = "";
        $database_name = "clinicmanagementsystem";
        $date_string   = "Backup_" . date("Y_m_d__h_i_s");

        $cmd = "C:/xampp/mysql/bin/mysqldump --routines -h {$server_name} -u {$username} {$database_name} > " . BACKUP_PATH . "{$date_string}_{$database_name}.sql";

        exec($cmd);
        sleep(2);
        $pathToFile = BACKUP_PATH . $date_string.'_'.$database_name.'.sql';
        return response()->download($pathToFile);
    }

    public function recovery()
    {
        return view('Patient.recover');
    }

    public function updateAppointment(Request $request)
    {
        //return $request;

        $id = $request->input('pid');

           foreach (Symptoms::where("p_id",$id)->get() as $sm) {

                $sp = Symptoms::findorfail($sm->id)->delete();
                }

            foreach (Test::where("p_id",$id)->get() as $sm) {

                $sp = Test::findorfail($sm->id)->delete();
                }

            foreach (Diet::where("p_id",$id)->get() as $sm) {

                $sp = Diet::findorfail($sm->id)->delete();
                }


            foreach (Diagnosis::where("p_id",$id)->get() as $sm) {

                $sp = Diagnosis::findorfail($sm->id)->delete();
                }


            foreach (PatientMedicine::where("p_id",$id)->get() as $sm) {

                $sp = PatientMedicine::findorfail($sm->id)->delete();
                }


    // add symptoms

       if (isset($request->s_title)) {
            foreach ($request->s_title as $key => $symp) {

                $sp = new Symptoms();
                $sp->p_id =$request->pid; 
                $sp->title = $request->s_title[$key];  
                $sp->value = $request->s_value[$key];  
                $sp->save();

                }
        }

   // patient test
    if (isset($request->t_title) && isset($request->t_value)) {
            foreach ($request->t_title as $key => $symp) {
                $test = new Test();
                $test->p_id =$request->pid; 
                $test->title = $request->t_title[$key];  
                $test->result = $request->t_value[$key];  
                $test->save();
              
                }
             
        }
    
    // patient medicine
    if (isset($request->m_title) && isset($request->m_quantity) && isset($request->m_time)) {
            foreach ($request->m_title as $key => $symp) {
                $med = new PatientMedicine();
                $med->p_id =$request->pid; 
                $med->title = $request->m_title[$key];  
                $med->quantity = $request->m_quantity[$key] ;
                $med->times = $request->m_time[$key];
                $med->save();
                }
        }

        //patient diagnoses
         if (isset($request->d_disease) && isset($request->d_comment)) {
            foreach ($request->d_disease as $key => $symp) {
                $dig = new Diagnosis();
                $dig->p_id =$request->pid; 
                $dig->disease = $request->d_disease[$key];  
                $dig->comment = $request->d_comment[$key];
                $dig->save();
                }
        }

        //patient diet 
    if (isset($request->d_name) && isset($request->d_quantity) && isset($request->d_times)) {
            foreach ($request->d_name as $key => $symp) {
                $diet = new Diet();
                $diet->p_id =$request->pid; 
                $diet->name = $request->d_name[$key];  
                $diet->quantity = $request->d_quantity[$key] ;
                $diet->times = $request->d_times[$key];
                $diet->save();

                }
        }

        $acc = Income::where('p_id', $request->input('pid'))->first();
        $acc->p_id = $request->p_id;
        $acc->amountReceived = $request->amountReceived;
        if ($request->amountReceivedAble == null) {
            $acc->amountReceivedAble = 0;
        }
        else
        $acc->amountReceivedAble = $request->amountReceivedAble;

        $acc->balance = $request->balance;
        $acc->save();


        return redirect(route('appointment-detail', $request->pid));
    }

    public function deletePatient($id)
    {
        $patient = Patient::findorfail($id);

        $apts = Appointment::all()->where('p_id', $id);

        foreach ($apts as $apt) {
            $this->deleteAppoint($apt->id);
        }

        $patient->delete();

        return view('Patient.patientList');
    }

    public function editPatient($id)
    {        
        $patient = Patient::findorfail($id);
        return view('Patient.editPatient')->with('patient', $patient);
    }

    public function editPatientPost(Request $request,$id)
    {
        

         $patient = Patient::findorfail($id);
         $imageName =$patient->image;
         if (isset($request->image)) {
               if($request->hasfile('image')){
                    $file=$request->image;
                    $file->move(public_path().'/PatientImages/', $file->getClientOriginalName());
                    $imageName= $file->getClientOriginalName();
                    }
         }
         $patient->name =$request->name;
         $patient->nic =$request->nic;
         $patient->age =$request->age;
         $patient->phone =$request->phone;
         $patient->image = $imageName;
         $patient->address =$request->address;
         $patient->address =$request->address;
         $patient->otherDetail =$request->otherDetail;
         $patient->update();

        return redirect(url('patients-list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Redietonse
     */

    public function submitRecovery(Request $request)
    {
        if($request->hasfile('db_back_up')) 
        {
            $file=$request->db_back_up;
            $file->move(public_path().'/recoveries/', $file->getClientOriginalName());
            $imageName= $file->getClientOriginalName();            

            $restore_file  = public_path() . '/recoveries/' . $file->getClientOriginalName();
            $server_name   = "localhost";
            $username      = "root";
            $password      = "";
            $database_name = "clinicmanagementsystem";

            $cmd = "C:\\xampp\mysql\bin\mysql -h {$server_name} -u {$username} {$database_name} < $restore_file";
            exec($cmd);

            return view('Patient.patientList')->with('recovery', ['message' => 'Recovery successful!']);

         }

    }

    public function savePatient(Request $request)
    {

    $imageName ="user.png";
     if($request->hasfile('image')){
        $file=$request->image;
        $file->move(public_path().'/PatientImages/', $file->getClientOriginalName());
        $imageName= $file->getClientOriginalName();
         }

         $patient = new Patient();
         $patient->name =$request->name;
         $patient->nic =$request->nic;
         $patient->age =$request->age;
         $patient->phone =$request->phone;
         $patient->image = $imageName;
         $patient->address =$request->address;
         $patient->address =$request->address;
         $patient->otherDetail =$request->otherDetail;
         $patient->save();

        return view("Patient.patientProfile")->with("patient",$patient);
    }

    public function createAppointment($id)
    {
        $patient = Patient::findorfail($id);
        return view('appointment.createAppointment')->with('patient',$patient); 
    }

    public function deletAppoint($id)
    {

        $app= Appointment::findorfail($id)->delete();
           foreach (Symptoms::where("p_id",$id)->get() as $sm) {

                $sp = Symptoms::findorfail($sm->id)->delete();
                }

            foreach (Test::where("p_id",$id)->get() as $sm) {

                $sp = Test::findorfail($sm->id)->delete();
                }

            foreach (Diet::where("p_id",$id)->get() as $sm) {

                $sp = Diet::findorfail($sm->id)->delete();
                }


            foreach (Diagnosis::where("p_id",$id)->get() as $sm) {

                $sp = Diagnosis::findorfail($sm->id)->delete();
                }


            foreach (PatientMedicine::where("p_id",$id)->get() as $sm) {

                $sp = PatientMedicine::findorfail($sm->id)->delete();
                }


            foreach (Income::where("p_id",$id)->get() as $sm) {

                $sp = Income::findorfail($sm->id);
                $sp->id=0;
                $sp->update();
                }

                return back();
    }

    public function deleteAppoint($id)
    {

        $app= Appointment::findorfail($id)->delete();
           foreach (Symptoms::where("p_id",$id)->get() as $sm) {

                $sp = Symptoms::findorfail($sm->id)->delete();
                }

            foreach (Test::where("p_id",$id)->get() as $sm) {

                $sp = Test::findorfail($sm->id)->delete();
                }

            foreach (Diet::where("p_id",$id)->get() as $sm) {

                $sp = Diet::findorfail($sm->id)->delete();
                }


            foreach (Diagnosis::where("p_id",$id)->get() as $sm) {

                $sp = Diagnosis::findorfail($sm->id)->delete();
                }


            foreach (PatientMedicine::where("p_id",$id)->get() as $sm) {

                $sp = PatientMedicine::findorfail($sm->id)->delete();
                }


            foreach (Income::where("p_id",$id)->get() as $sm) {

                $sp = Income::findorfail($sm->id);
                $sp->id=0;
                $sp->update();
                }

                return 1;
    }
    

    public function saveAppointment(Request $request)
    {

        $ap =  new Appointment();
        $ap->title = $request->title;
        $ap->date = $request->date;
        $ap->p_id = $request->p_id;
        $ap->save();
        $request->p_id = $ap->id;
    

    // add symptoms

       if (isset($request->s_title)) {
            foreach ($request->s_title as $key => $symp) {

                $sp = new Symptoms();
                $sp->p_id =$request->p_id; 
                $sp->title = $request->s_title[$key];  
                $sp->value = $request->s_value[$key];  
                $sp->save();

                }
        }

   // patient test
    if (isset($request->t_title) && isset($request->t_value)) {
            foreach ($request->t_title as $key => $symp) {
                $test = new Test();
                $test->p_id =$request->p_id; 
                $test->title = $request->t_title[$key];  
                $test->result = $request->t_value[$key];  
                $test->save();
              
                }
             
        }
    
    // patient medicine
    if (isset($request->m_title) && isset($request->m_quantity) && isset($request->m_time)) {
            foreach ($request->m_title as $key => $symp) {
                $med = new PatientMedicine();
                $med->p_id =$request->p_id; 
                $med->title = $request->m_title[$key];  
                $med->quantity = $request->m_quantity[$key] ;
                $med->times = $request->m_time[$key];
                $med->save();
                }
        }

        //patient diagnoses
         if (isset($request->d_disease) && isset($request->d_comment)) {
            foreach ($request->d_disease as $key => $symp) {
                $dig = new Diagnosis();
                $dig->p_id =$request->p_id; 
                $dig->disease = $request->d_disease[$key];  
                $dig->comment = $request->d_comment[$key];
                $dig->save();
                }
        }

        //patient diet 
    if (isset($request->d_name) && isset($request->d_quantity) && isset($request->d_times)) {
            foreach ($request->d_name as $key => $symp) {
                $diet = new Diet();
                $diet->p_id =$request->p_id; 
                $diet->name = $request->d_name[$key];  
                $diet->quantity = $request->d_quantity[$key] ;
                $diet->times = $request->d_times[$key];
                $diet->save();

                }
        }

        $acc = new  Income();
        $acc->p_id = $request->p_id;
        $acc->amountReceived = $request->amountReceived;
        if ($request->amountReceivedAble == null) {
            $acc->amountReceivedAble = 0;
        }
        else
        $acc->amountReceivedAble = $request->amountReceivedAble;

        $acc->balance = $request->balance;
        $acc->save();

            return redirect(url("patients-list"));
    }


    public function appointmentDetail($id)
    {
        $appointment = Appointment::findorfail($id);
        return view('Patient.appointmentDetail',compact('appointment'));
    }

    public function editAppoint($id)
    {
        $appointment = Appointment::findorfail($id);
        $patient = Patient::find($appointment->p_id);

        return view('Patient.appointmantEdit')->with(['patient' => $patient, 'appointment' => $appointment]);        
    }

    public function patientDetail($id)
    {
        $patient= Patient::findorfail($id);
        return view("Patient.patientProfile",compact('patient'));
    }

    public function deleteExp($id)
    {
        $exp = Expenses::findorfail($id);
        $exp->delete();
        return redirect(route('expense-list'));
    }

    public function expList()
    {
        return view('expenses.expList');
    }

    public function addExpForm()
    {
        return view('expenses.addExp');
    }

    public function saveExp(Request $request)
    {
        Expenses::create($request->all());
        return redirect(url('expense-list'));

    }
}
