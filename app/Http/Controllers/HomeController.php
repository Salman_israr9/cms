<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Test;
use App\User;
use Hash;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     
    public function __construct()
    {
        $this->middleware('auth');
    }
    */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function addMember()
    {
        return view('member');        
    }

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        return view('Patient.addPatient');
    }

    public function userUpdate(Request $request)
    {
      

        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|',
        ]);

        $newPassword = $request->password_confirmation;


        $pwdHash = Hash::make($newPassword);

        $servername = "localhost";

        $username = "root";
        
        $password = "";
        
        $dbname = "clinicmanagementsystem";

        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        } 

        $sql = "UPDATE users SET password='" . $pwdHash . "' WHERE id = " . $request->id;

        if ($conn->query($sql) === TRUE) 
        {
            echo "Record updated successfully";
        } else {
            echo "Error updating record: " . $conn->error;
        }

        $conn->close();
        return $request;
    }    

}
