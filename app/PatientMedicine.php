<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientMedicine extends Model
{
    protected $fillable =['p_id','title','quantity','times'];
}
