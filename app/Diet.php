<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diet extends Model
{
    
    protected $fillable =["p_id","name","times"];
}
