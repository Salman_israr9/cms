<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Income extends Model
{
    protected $fillable = ['p_id','amountReceived','amountReceivedAble','balance'];

}
