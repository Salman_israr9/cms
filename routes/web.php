<?php



//Route::get("/dashboard","PatientController@dashboard")->name("dashboard");
// patients routes
Route::get('/', 'PatientController@patientList')->name('/');
Route::get("add-patient","PatientController@addPatient")->name("add-patient");
Route::post("submit-patient","PatientController@savePatient")->name("submit-patient");
Route::get("patients-list","PatientController@patientList")->name("patients-list");

Route::get('/dashboard',"PatientController@dashboard");

Route::get("delete-patient/{id}","PatientController@deletePatient")->name("delete-patient");
Route::get("edit-patient/{id}","PatientController@editPatient")->name("edit-patient");
Route::post("edit-post-patient/{id}","PatientController@editPatientPost")->name("edit-post-patient");

// symptoms routes
Route::get("symptoms-list","MedicineController@symptomList");
Route::get("add-symptom","MedicineController@addSymptomForm");
Route::post("save-symptom","MedicineController@saveSymptom");
Route::get("edit-symptom/{id}","MedicineController@editSymptomForm");
Route::post("update-symptom/{id}","MedicineController@updateSymptom");
Route::get("delete-symptom/{id}","MedicineController@deleteSymptom");
Route::post('update-appointment', "PatientController@updateAppointment")->name('update-appointment');
// create appointment


Route::get('create-appointment/{id}','PatientController@createAppointment')->name('createAppointment');

Route::post('save-appointment','PatientController@saveAppointment')->name('save-appointment');

Auth::routes();

Route::get('/home', function(){ return view('layouts.overview');})->name('home');
Route::get('appointment-detail/{id}','PatientController@appointmentDetail')->name("appointment-detail");

Route::get("delete-appointment/{id}","PatientController@deletAppoint")->name('delete-appointment');
Route::get("edit-appointment/{id}","PatientController@editAppoint")->name('edit-appointment');
Route::get("patient-detail/{id}","PatientController@patientDetail")->name('patient-detail');

Route::get('add-expense-form',"PatientController@addExpForm")->name('add-expense-form');
Route::get('delete-expense/{id}',"PatientController@deleteExp")->name('delete-expense');
Route::get('expense-list',"PatientController@expList")->name('expense-list');

Route::get('backup-data', "PatientController@backup")->name('backup-data');
Route::get('recover-data', "PatientController@recovery")->name('recover-data');

Route::post('save-expense','PatientController@saveExp')->name('save-expense');

Route::post('submit-recovery', "PatientController@submitRecovery")->name('submit-recovery');

Route::get("registered",function(){


	return view("layouts.dashboard");
});

Route::post("user-update","HomeController@userUpdate")->name("user-update");

Route::get('bogbira', function(){

    return view('auth.passwords.reset');

});

Route::post('bochi', 'Auth\ResetPasswordController@bochi')->name('bochi');

Route::get('member', function(){

	return view('member');

});
Route::post('add-user', 'Auth\RegisterController@create')->name('add-user');

// Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
// Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
// Route::get('password/reset/{email}', 'Auth\ResetPasswordController@showResetForm');
// Route::post('password/reset', 'Auth\ResetPasswordController@reset');
