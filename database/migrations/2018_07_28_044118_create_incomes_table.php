<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncomesTable extends Migration
{



    public function up()
    {
        Schema::create('incomes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('p_id')->default(0);
            $table->double('amountReceived');
            $table->double('amountReceivedAble')->default(00.00);
            $table->double('balance');
            $table->timestamps();
        });
    }


    
    public function down()
    {
        Schema::dropIfExists('incomes');
    }
}
