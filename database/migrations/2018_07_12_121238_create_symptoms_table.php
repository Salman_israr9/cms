<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSymptomsTable extends Migration
{
    


    public function up()
    {
        Schema::create('symptoms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('p_id');
            $table->string('title');
            $table->string('value');
            $table->timestamps();
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('symptoms');
    }
}
