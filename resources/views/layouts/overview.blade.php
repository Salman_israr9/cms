@extends('layouts.app')


@section('overview')

 <section class="content-header">
      <h1>
        Dashboard
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->

          @php $plist = App\Patient::whereDate('created_at', Carbon\Carbon::today())->get() @endphp

          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{count($plist)}}</h3>

              <p>New Patients</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
          @php $aptlist = App\Appointment::whereDate('created_at', Carbon\Carbon::today())->get() @endphp
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{count($aptlist)}}<sup style="font-size: 20px"></sup></h3>

              <p>Appointments</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->

        @php 

          $incomelist = App\Income::whereDate('created_at', Carbon\Carbon::today())->get();
          $sum = 0;
          foreach($incomelist as $ii)
          {
            $sum = $sum + $ii->balance;
          }
        @endphp
    
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>Rs:{{$sum}}</h3>

              <p>Today Income</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
         @php 

          $expenselist = App\Expenses::whereDate('created_at', Carbon\Carbon::today())->get();
          $sum = 0;
          foreach($expenselist as $ii)
          {
            $sum = $sum + $ii->amount;
          }
        @endphp

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>Rs:{{$sum}}</h3>

              <p>Today Expense</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="{{route('expense-list')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row container">
      	@yield('content')
      </div>
      <!-- /.row (main row) -->

    </section>

@stop