@extends('layouts.app')

@section('content')

<div class="container">
	


	<div class="card">
		<div class="card-header">
<!-- 			<h2><b>Profile</b></h2>
 -->		</div>
		<hr>
		<div class="" style="background: #fff;padding: 20px;">
			<div class="row" style="position: relative;">
					
				<img src="{{asset('PatientImages/'.$patient->image)}}" style="width: 130px;height: 130px;box-shadow: 0 0 26px -6px #000;float: left;" class="img-circle">
				
				<div style="position: absolute;left:150px;top:40px">
					<div style="color: silver;font-size: 24px;font-family: serif;">
						<b>{{$patient->name}} ({{$patient->age}})</b>
					</div>
					<div>
						<b>{{$patient->phone}}</b>
					</div>
				</div>
			</div>
		</div>		
	</div>

	@php $apts = App\Appointment::all()->where('p_id', $patient->id); @endphp
	
	<ul class="nav nav-tabs">
	  @if(!$apts->isEmpty())			
	  <li class="active"><a data-toggle="tab" href="#home">Appointments</a></li>
	  @endif
	  <li @php if($apts->isEmpty()) echo 'class="active"'; @endphp ><a data-toggle="tab" href="#new">New</a></li>
	</ul>

	@if(!$apts->isEmpty())			
	<div class="tab-content" style="background: #fff">
	  <div id="home" class="tab-pane fade in active">
			<table class="table table-success">
				<thead>
					<th>Title</th>
					<th>Date</th>
					<th>Detail</th>
				</thead>
				<tbody>

					@foreach($apts as $p)
					<tr>
						<td>{{$p->title}}</td>
						<td>{{$p->date}}</td>
						<td>
							<a href="{{url('appointment-detail',$p->id)}}" class="btn btn-info">Detail</a>
							<a href=" {{url('delete-appointment',$p->id)}}" class="btn btn-danger del">Delete</a>
							<a href=" {{url('edit-appointment',$p->id)}}" class="btn btn-primary ">Edit</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			 </table>
	  </div>
	  @else 

	  @endif

	  <div id="new" class="tab-pane fade @php if($apts->isEmpty()) echo 'in active'; @endphp " style="padding: 30px;">
	  	<a href="{{url('create-appointment',$patient->id)}}" class="btn btn-primary" >Add Appointment</a>
	  </div>

	</div>

	</div>

	<script type="text/javascript">
		$(document).ready(function(){
  $(".del").click(function(){
    if (!confirm("Do you want to delete")){
      return false;
    }
  });
});
	</script>

@stop