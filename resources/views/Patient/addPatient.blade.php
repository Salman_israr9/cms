@extends('layouts.app')
@section('content')
<style type="text/css">
	input
	{
		width: 300px;
	}
</style>
<div class="panel panel-primary" style="margin:2%"">

	<h2 class="panel-heading"><b>Add Patient</b></h2>

	<form action="{{route('submit-patient')}}" method="post" enctype="multipart/form-data" class="panel-body">
		{{csrf_field()}}
	<div class="row" style="margin-left: 20%; width: 50%" >
	

		<span class="input-group-text" id="inputGroup-sizing-lg"><b>Patient's  Name 
    			<span style="color: red;">*</span></b></span>
    			<input type="text" required name="name" class="form-control"  step="width:200px">
		<br>

	    	<span class="input-group-text" id="inputGroup-sizing-lg"><b>Phone :</b>
	    		<span style="color: red;">*</span>
	    	</span>
	  			<input type="text" name="phone" required class="form-control" >
		
		<br>
		
    		<span class="input-group-text" id="inputGroup-sizing-lg"><b>Address :</b>
    			<span style="color: red;">*</span>
    		</span>
  			<input type="text" required  name="address" class="form-control" >
  		<br>

    		<span class="input-group-text" id="inputGroup-sizing-lg"><b>Patient's Age</b>
				<span style="color: red;">*</span>
    		</span>
  			<input type="number" required name="age" class="form-control">
		<br>
    		<span class="input-group-text" id="inputGroup-sizing-lg"><b>Patient's CNIC</b>
    			<span>(opt)</span>
    		</span>
  			<input type="text" name="nic" class="form-control" >
	
		<br>
    		<span class="input-group-text" id="inputGroup-sizing-lg"><b>Blood Group :</b></span>
  			<select name="bloodGroup" class="form-control">
  				<option value="A+">A+</option>
  				<option value="B+">B+</option>
  				<option value="AB+">AB+</option>
  				<option value="A-">A-</option>
  				<option value="B">B-</option>
  				<option value="AB-">AB-</option>
  				<option value="O+">O+</option>
  				<option value="O-">O-</option>
  				<option value="notknow">Not know</option>
  			</select>
		<br>

    		<span class="input-group-text" id="inputGroup-sizing-lg"><b>Pateint's Other Detail</b>(opt) : </span>
  		
  			<textarea  class="form-control" style="resize: none;" name="OtherDetail" cols="10"></textarea> 

		<br>
		
		    <span class="input-group-text"><b>Picture (optional)</b></span>
		    <input type="file" name="image" id="profile-img" style="display: inline;">
		    <img src="" id="profile-img-tag" width="20%" height="30%" style="margin-left: 60%; display: inline;" />
		<br>
		<br>
		<input type="submit" name="submit"  class="form-control btn btn-primary  col-md-6">
   </div>
	</form>

	 <script src="{{asset('js/picshow.js')}}"></script>

<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#profile-img").change(function(){
        readURL(this);
    });
</script>
</div>

@endsection
