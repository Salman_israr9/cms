@extends('layouts.app')

@section('content')

	<div class="row">
		<div class="col-sm-3">
		</div>

		<div class="col-sm-6">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h1>Select file to recover</h1>
				</div>
				<div class="panel-body">
					<form action="submit-recovery" method="post" enctype="multipart/form-data" >
						@csrf
						<input type="file" value="Select file to recover" name="db_back_up" class="form-control" accept=".sql"> 
						<hr>
						<input type="submit" value="Recover" class="btn btn-primary">
					</form>
				</div>
			</div>
		</div>
	</div>

@endsection