@extends('layouts.app')


@section('content')
<style>
	thead{background:gray}
</style>
<div id="appt-table" style="margin-left: 2%; margin-right: 2%">
	<h1> <span style="color:green">{{$appointment->title}}</span> <span style="font-size: 20px; text-align: right; margin-right: 10%">({{$appointment->date}})</span></h1>	
<hr>

	<div>
		<h2>Symptoms :</h2>
		<table class="table table-success">
			<thead>
				<th>Title</th>
				<th>Value</th>
			</thead>
			<tbody>
				@foreach(App\Symptoms::where("p_id",$appointment->id)->get() as $s)
				 <tr>
				 	<td>{{$s->title}}</td>
				 	<td>{{$s->value}}</td>
				 </tr>
				@endforeach
			</tbody>
		</table>
	</div>

	<div>
		<h2>Test :</h2>
		<table class="table table-success">
			<thead>
				<th>Title</th>
				<th>Result</th>
			</thead>
			<tbody>
				@foreach(App\Test::where("p_id",$appointment->id)->get() as $s)
				 <tr>
				 	<td>{{$s->title}}</td>
				 	<td>{{$s->result}}</td>
				 </tr>
				@endforeach
			</tbody>
		</table>
	</div>


	<div>
		<h2>Medicine :</h2>
		<table class="table table-success">
			<thead>
				<th>Title</th>
				<th>Quantity</th>
				<th>Times</th>
			</thead>
			<tbody>
				@foreach(App\PatientMedicine::where("p_id",$appointment->id)->get() as $s)
				 <tr>
				 	<td>{{$s->title}}</td>
				 	<td>{{$s->quantity}}</td>
				 	<td>{{$s->times}}</td>
				 </tr>
				@endforeach
			</tbody>
		</table>
	</div>

	<div>
		<h2>Diet :</h2>
		<table class="table table-success">
			<thead>
				<th>Title</th>
				<th>Quantity</th>
				<th>Times</th>
			</thead>
			<tbody>
				@foreach(App\Diet::where("p_id",$appointment->id)->get() as $s)
				 <tr>
				 	<td>{{$s->name}}</td>
				 	<td>{{$s->quantity}}</td>
				 	<td>{{$s->times}}</td>
				 </tr>
				@endforeach
			</tbody>
		</table>
	</div>



	<div>
		<h2>Dignoses Disease :</h2>
		<table class="table table-success">
			<thead>
				<th>Disease</th>
				<th>Comment</th>
			</thead>
			<tbody>
				@foreach(App\Diagnosis::where("p_id",$appointment->id)->get() as $s)
				 <tr>
				 	<td>{{$s->disease}}</td>
				 	<td>{{$s->Comment}}</td>
				 </tr>
				@endforeach
			</tbody>
		</table>
	</div>


	<div>
		<h2>Account :</h2>
		<table class="table table-success">
			<thead>
				<th>Amount Received</th>
				<th>Amount Receiveable</th>
				<th>Balance</th>
			</thead>
			<tbody>
				@foreach(App\Income::where("p_id",$appointment->id)->get() as $s)
				 <tr>
				 	<td>{{$s->amountReceived}}</td>
				 	<td>{{$s->amountReceivedAble}}</td>
				 	<td>{{$s->balance}}</td>
				 </tr>
				@endforeach
			</tbody>
		</table>
	</div>

	
	<a href="#" onclick="printAppointment()" class="btn btn-primary">Print</a>

	<hr>

	<div id="printArea" style="display: none;">
		<div>

			<div class="row">
			<div class="col-xs-5">
				<h3>Dr. Muhammad Maqsood</h3>
				<h5>M.B.B.S (Pb) F.C.P.S (General Medicine)</h5>
				<h5>Consultant Physician / Assistant Professor (Medicine)</h5>
				<h5>Lahore General Hospital</h5>
				<h5>Cell: 0321-8419257</h5>
				<h5>Email: maqsooddrmc@hotmail.com</h5>			
			</div>
			
			<div class="col-xs-3">
				<img src='{{asset('/user/logo.jpg')}}' style="text-align: center; page-break-before: auto;page-break-after: auto;     page-break-inside: avoid; " width="200" height="200">		
			</div>

			<div class="col-xs-4" style="direction: rtl; font-family: Jameel Noori Nastaleeq">
				<h3>ڈاکٹر محمد مقصود</h3>
				<h5>ایم بی بی ایس، ایف سی پی ایس (جنرل میڈیسن)</h5>
				<h5>کنسلٹنٹ فزیشن/ اسسٹنٹ پروفیسر (میڈیسن)</h5>
				<h5>شوگر معدہ اور جگر سپیشلسٹ</h5>
				<h5>لاہور جنرل ہسپتال</h5>
			</div>

			</div>

			<hr style="border-width: 2px;">
			<div style="font-size: 18px;" class="pateint-details">
				@php  $pt = App\Patient::findorfail($appointment->p_id); @endphp

				<div class="row">

					<div class="col-xs-4">						
						Patient' Name: <b style="border-bottom: 1px solid black; padding-bottom: 3px">{{$pt->name}}</b> 
					</div>

					<div class="col-xs-4">
						Mobile: <b style="border-bottom: 1px solid black; padding-bottom: 3px">{{$pt->phone}}</b>						
					</div>

					<div class="col-xs-4 row">

						<div class="col-xs-5"> 
							Age: <b style="border-bottom: 1px solid black; padding-bottom: 3px">{{$pt->age}}</b> 		
						</div>
						
						<div class="col-xs-7">						Appointment Id: <b style="border-bottom: 1px solid black; padding-bottom: 3px">{{$appointment->id}}</b>							
						</div>
					
					</div>

				</div>
			</div>
			<hr>
			<div style="margin: 10px 9%">
				@php $symp = App\Symptoms::where("p_id",$appointment->id)->get(); @endphp
				@if(!$symp->isEmpty())
				<div>
		<h4>Symptoms :</h4>
		<table class="table table-success">
			<thead>
				<th>Title</th>
				<th>Value</th>
			</thead>
			<tbody>
				@foreach($symp as $s)
				 <tr>
				 	<td>{{$s->title}}</td>
				 	<td>{{$s->value}}</td>
				 </tr>
				@endforeach
			</tbody>
		</table>
	</div>
	@endif

	@php $tests = App\Test::where("p_id",$appointment->id)->get() @endphp
	@if(!$tests->isEmpty())
	<div>
		<h4>Test :</h4>
		<table class="table table-success">
			<thead>
				<th>Title</th>
				<th>Result</th>
			</thead>
			<tbody>
				@foreach($tests as $s)
				 <tr>
				 	<td>{{$s->title}}</td>
				 	<td>{{$s->result}}</td>
				 </tr>
				@endforeach
			</tbody>
		</table>
	</div>
	@endif

	@php $medi = App\PatientMedicine::where("p_id",$appointment->id)->get() @endphp
	@if(!$medi->isEmpty())
	<div>
		<h4>Medicine :</h4>		
		<table class="table table-success">
			<thead>
				<th>Title</th>
				<th>Quantity</th>
				<th>Times</th>
			</thead>
			<tbody>
				@foreach($medi as $s)
				 <tr>
				 	<td>{{$s->title}}</td>
				 	<td>{{$s->quantity}}</td>
				 	<td>{{$s->times}}</td>
				 </tr>
				@endforeach
			</tbody>
		</table>
	</div>
	@endif

	@php $diet = App\Diet::where("p_id",$appointment->id)->get() @endphp
	@if(!$diet->isEmpty())
	<div>
		<h4>Diet :</h4>
		<table class="table table-success">
			<thead>
				<th>Title</th>
				<th>Quantity</th>
				<th>Times</th>
			</thead>
			<tbody>
				@foreach($diet as $s)
				 <tr>
				 	<td>{{$s->name}}</td>
				 	<td>{{$s->quantity}}</td>
				 	<td>{{$s->times}}</td>
				 </tr>
				@endforeach
			</tbody>
		</table>
	</div>
	@endif

	@php $diag = App\Diagnosis::where("p_id",$appointment->id)->get() @endphp
	@if(!$diag->isEmpty())
	<div>
		<h4>Dignoses Disease :</h4>
		<table class="table table-success">
			<thead>
				<th>Disease</th>
				<th>Comment</th>
			</thead>
			<tbody>
				@foreach($diag as $s)
				 <tr>
				 	<td>{{$s->disease}}</td>
				 	<td>{{$s->Comment}}</td>
				 </tr>
				@endforeach
			</tbody>
		</table>
	</div>
	@endif

	@php $acc = App\Income::where("p_id",$appointment->id)->get()  @endphp
	@if(!$acc->isEmpty())
	<div>
		<h4>Account :</h4>
		<table class="table table-success">
			<thead>
				<th>Amount Received</th>
				<th>Amount Receiveable</th>
				<th>Balance</th>
			</thead>
			<tbody>
				@foreach($acc as $s)
				 <tr>
				 	<td>{{$s->amountReceived}}</td>
				 	<td>{{$s->amountReceivedAble}}</td>
				 	<td>{{$s->balance}}</td>
				 </tr>
				@endforeach
			</tbody>
		</table>
	</div>
	@endif
			</div>

			<div style="text-align: right;margin-top: 50px;margin-right: 10px "> Signature: _____________________</div>

		</div>

	</div>

</div>

<script>

	function printAppointment() {
	     var printContents = document.getElementById('printArea').innerHTML;
	     var originalContents = document.body.innerHTML;
	     //$("#body-boga").text(e_new.html());
	     document.body.innerHTML = printContents;

	     window.print();
	     //$("#body-boga").empty();
	     document.body.innerHTML = originalContents;
	}

</script>

@stop
