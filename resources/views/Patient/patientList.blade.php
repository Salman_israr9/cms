@extends("layouts.app")
@section('content')

<div class="" style="padding: 10px;">
  
  @if(!empty($recovery))
    <h3 class="alert alert-success">{{$recovery['message']}}</h3>
  @endif

	<div class="card">
  <div class="card-header">
  	<h2 style="display: inline;"><b>Patient's List </b> <a class="fa fa-plus myroundminus" href="{{route('add-patient')}}"  ></a></h2>

    </div>
  <div class="card-body">
    		<hr>
      <!--   <input placeholder="Search name " class="form-control" type="text" id="myInput"  onkeyup="myFunction(this)">
       -->  
        @php $pats = App\Patient::all();  @endphp
        @if(!$pats->isEmpty())


    	<table class="table table-hover" id="myTable">

		<thead style="background: #17a2b8">
			<th> 
        <input placeholder="Name" class="" type="search" id="myInputName"  onkeyup="myFunctionName(this)">
      </th>
			<th>        
        <input placeholder="Phone" class="" type="text" id="myInputPhone"  onkeyup="myFunctionPhone(this)">
      </th>
      <th>        
        CNIC
      </th>
			<th>Address</th>
      <th>Delete</th>
      <th>Edit</th>
		</thead>
		<tbody>
        
			@foreach($pats as $patient)
			<tr>
				<td><a href="{{route('patient-detail', $patient->id)}}">{{$patient->name}}</a></td>
				<td>{{$patient->phone}}</td>
        <td>{{$patient->nic}}</td>
				<td>{{$patient->address}}</td>
        <td>
          <a href="{{route('delete-patient',$patient->id)}}" onclick="return confirm('Are you sure?')" class="btn btn-danger"><span class="fa fa-remove"></span></a>
        </td>
        <td>
          <a href="{{route('edit-patient',$patient->id)}}" class="btn btn-primary"><span class="fa fa-edit"></span></a>
        </td>
			</tr>
			@endforeach
		</tbody>
	</table>
  @else
      <a href="{{route('add-patient')}}" class="btn btn-success">Add Patient</a>
  @endif
  </div>
</div>


<script>


function myFunctionName() {
  // Declare variables 
  var input, filter, table, tr, td, i;
  input = document.getElementById('myInputName');
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");

  // Loop ssaathrough all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    } 

  }
}



function myFunctionPhone() {
  // Declare variables 
  var input, filter, table, tr, td, i;
  input = document.getElementById('myInputPhone');
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");

  // Loop ssaathrough all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    } 

  }
}


</script>

</div>

<style type="text/css">
  .myroundminus
  {
    box-shadow: 0px 0px 23px -6px #000;
    background: white;
    border-radius: 19px;
      display: inline-block;
      width: 31px;
      height: 31px;
      padding: 3px;
      text-decoration: none;
  }

  .myroundminus:hover{
    background: #eee;
  }
</style>

@endsection