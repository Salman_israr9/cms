@extends("layouts.app")
@section('content')
<div class="container">
	<div style="margin-left: 90%;margin-bottom: 2%">
	<a href="{{url("add-symptom")}}" class="btn btn-success">Add</a>
	</div>
	<div class="card">
  <div class="card-header">
  	<h2 style="display: inline;"><b>Symptom's List </b></h2>
  	<div style="float: right;">
  		<input type=""  class="" name="">
  		<input type="button" class="btn btn-info" value="Search"  name="">
  	</div>

    </div>
  <div class="card-body">
    	<table class="table table-hover">
		<thead style="background: #17a2b8">
			<th>Title</th>
			<th>Value</th>
			<th>Action</th>
		</thead>
		<tbody>
			@foreach(App\Symptoms::all() as $symptom)
			<tr>
				<td>{{$symptom->title}}</td>
				@if($symptom->valueType == "textbox")
				<td><input type="" name=""></td>
				@else
				<td><input type="checkbox" name=""></td>
				@endif
				<td>
					<a href="{{url('edit-symptom',$symptom->id)}}" class="btn btn-primary">Edit</a>

				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
  </div>
</div>


</div>

@endsection