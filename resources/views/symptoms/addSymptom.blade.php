@extends('layouts.app')
@section('content')
<div class="container">
	<form class="" action="{{url('save-symptom')}}" method="post">
		{{csrf_field()}}
		<div class="form-group">
			<label class="cotrol-label"><b><h2>Title</h2></b></label><br>
		<input type="text"  name="title" class="form-control" required>
		</div>
		<div class="form-group">
			<label class="cotrol-label"><b><h2>Value</h2></b></label><br>
			<input type="text"  name="value" class="form-control" required>
		</div>
		<div class="form-group">
			<input type="submit"  name="submit" value="submit"  class="form-control btn btn-primary">
		</div>

	</form>
	
</div>

@stop