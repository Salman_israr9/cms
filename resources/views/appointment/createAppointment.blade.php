@extends('layouts.app')
@section('content')

<div class="panel panel-primary" style="margin: 8px 45px;">
	<h2  class="panel-heading" >Creating appointment for  <span style="color: black;font-weight: bold;">{{$patient->name}}</span></h2>
	<hr>
	<style> 

   input
   {
   	border: 0;
   	outline: none;
   }
   </style>
	<form action="{{url('save-appointment')}}" method="post" class="panel-body">
		{{csrf_field()}}
		<div class="form-group row" style="">		   
		    <div class="col-sm-4" style="margin-left: 20px">
  				<label style="font-size: 20px">Title :</label>
  				@php $apt = App\Appointment::max('id') @endphp
				<input required type="text" value="Appointment {{$apt + 1}}" name="title" style="padding: 6px 11px;  width: 190px" class="mytextbox">
		    </div>
			<div class="col-sm-6 pull-right" style="margin-right: 40px">
		    	<label style="font-size: 20px;">Date :</label>
				<input class="mytextbox"  style="padding: 6px 11px; " required type='text' name="date" value="<?php echo date("m/d/Y"); ?>" >
			</div>
		</div>

		<hr>

		<input type="hidden" value="{{$patient->id}}"  name="p_id">

		<div class="row">
			<div class="col-sm-1">
			</div>

			<div class="col-sm-10">
					<div class="panel panel-info">
	      <div class="panel-heading">				
	      	<h4>Symptoms 
	      		<span  style="font-size: 30px; text-align: right; color: green;cursor: pointer;" onclick="addSymp()">+</span> 
	      	</h4> 
	      </div>
	      <div class="panel-body">
	      		<div id="symp">
				</div>
	      </div>
	    </div>


		<div class="panel panel-info">
	      <div class="panel-heading">				
	      	<h4>Test 
	      		<span  style="font-size: 30px; text-align: right; color: green;cursor: pointer;" onclick="addTest()">+</span> 
	      	</h4> 
	      </div>
	      <div class="panel-body">
	      		<div id="test">
				</div>
	      </div>
	    </div>


		<div class="panel panel-info">
	      <div class="panel-heading">				
	      	<h4>Medicine 
	      		<span  style="font-size: 30px; text-align: right; color: green;cursor: pointer;" onclick="addMed()">+</span> 
	      	</h4> 
	      </div>
	      <div class="panel-body">
	      		<div id="medicine">
				</div>
	      </div>

	    </div>

		<div class="panel panel-info">
	      <div class="panel-heading">				
	      	<h4>Diagnosis 
	      		<span  style="font-size: 30px; text-align: right; color: green;cursor: pointer;" onclick="addDiag()">+</span> 
	      	</h4> 
	      </div>
	      <div class="panel-body">
	      		<div id="diagnose">
				</div>
	      </div>

	    </div>


		<div class="panel panel-info">
	      <div class="panel-heading">				
	      	<h4>Diet 
	      		<span  style="font-size: 30px; text-align: right; color: green;cursor: pointer;" onclick="addDiet()">+</span> 
	      	</h4> 
	      </div>
	      <div class="panel-body">
	      		<div id="diet">
				</div>
	      </div>

	    </div>
			</div>

			<div class="col-sm-1">
			</div>
		</div>

		<hr>
		<div class="form-group">

		 <label>Amount Received :</label>
		 <input type="number" step="any" name="amountReceived" id="receive" required onkeyup="makeBal()">
		 <label style="color: red">Amount Receiveable :</label>
		 <input type="number" step="any" name="amountReceivedAble" required placeholder="00.00" id="r_able" onkeyup="makeBal()">
		 <label> Balance:</label>
		 <input type="number" step="any" name="balance" readonly id="bal">

		</div>	
		<div class="form-group">
		<input required type="submit" name="" value="Submit" class="btn btn-primary form-control">
		</div>
	</form>	
</div>

<script>


	function makeBal()
	{
	var re =	$("#receive").val();
	var r_a =	$("#r_able").val();
	var bal=	$("#bal").val(re-r_a);

	//alert(re);
	}

	function menus(t){
		$(t).parent().remove();
			}

	function addTest()
	{
		var testdiv = $("<div class ='testdiv row'>");

		var testdiv0 = $("<div class ='col-sm-5'>")
		testdiv0.append($("<label  style='font-size:19px;'>").text('Title'));
		testdiv0.append($("<input class='mytextbox' required name='t_title[]' placeholder=''>"));
		
		var testdiv1 = $("<div class ='col-sm-5'>");
		testdiv1.append($("<label style='font-size:19px; margin-left:2%'>").text('Result:'));
		testdiv1.append($("<input class='mytextbox' placeholder='' name='t_value[]'>"));

		testdiv.append(testdiv0);
		testdiv.append(testdiv1);
		testdiv.append($("<span class='fa fa-minus myroundminus' onclick='menus(this)' style='font-size:19px;cursor:pointer; color:red'>").text(""));
		testdiv.append($("<br>"));
		$("#test").append(testdiv);
	}


	function addDiet(){
		var dietdiv = $("<div class ='dietdiv row'>");
		
		var dietdiv1 = $("<div class ='col-sm-3'>");
		dietdiv1.append($("<label  style='font-size:13px;'>").text('Name'));
		dietdiv1.append($("<input required class='mytextbox' name='d_name[]' style='width:120px;'>"));
		
		var dietdiv2 = $("<div class ='col-sm-3'>");
		dietdiv2.append($("<label style='font-size:13px; margin-left:2%'>").text('Qty'));		
		dietdiv2.append($("<input required class='mytextbox' name='d_quantity[]' type='number' style='width:120px;' step ='any'>"));


		var dietdiv3 = $("<div class ='col-sm-3'>");
		dietdiv3.append($("<label style='font-size:13px; margin-left:2%'>").text('Times:'));		
		dietdiv3.append($("<input required class='mytextbox' name='d_times[]' style='width:110px;'>"));

		var dietdiv4 = $("<div class ='col-sm-2'>");
		var sel = $("<select class='d_duration'>");

		sel.append($("<option>").text("daily"))
		sel.append($("<option>").text("hourly"))
		dietdiv4.append($("<label style='font-size:13px; margin-left:2%'>").text('Routine'));		
		dietdiv4.append(sel);

		dietdiv.append(dietdiv1);
		dietdiv.append(dietdiv2);
		dietdiv.append(dietdiv3);
		dietdiv.append(dietdiv4);

		dietdiv.append($("<span class='fa fa-minus myroundminus' onclick='menus(this)' style='font-size:18px;margin-left:2%; color:red; cursor:pointer'>").text(""));
		dietdiv.append($("<br>"));
		$("#diet").append(dietdiv);
	}


	function addMed(){
		var meddiv = $("<div class ='meddiv row'>");

		var meddiv1 = $("<div class ='col-sm-4' style='margin:3px;'>")
		meddiv1.append($("<label  style='font-size:13px;'>").text('Title'));
		meddiv1.append($("<input required name='m_title[]' class='mytextbox' style='width:120px;' placeholder=''>"));
		
		var meddiv2 = $("<div class ='col-sm-4' style='margin:3px;'>")
		meddiv2.append($("<label style='font-size:13px; margin-left:2%'>").text('Qty'));
		meddiv2.append($("<input required name='m_quantity[]' class='mytextbox' style='width:120px;' placeholder='' >"));
		

		var meddiv3 = $("<div class ='col-sm-3' style='margin:3px;'>");
		meddiv3.append($("<label style='font-size:13px; margin-left:2%'>").text('Time'));
		meddiv3.append($("<input required name='m_time[]' class='mytextbox' style='width:120px;' placeholder='' >"));
		
		meddiv.append(meddiv1);
		meddiv.append(meddiv2);
		meddiv.append(meddiv3);
		meddiv.append($("<span class='fa fa-minus myroundminus' onclick='menus(this)' style='font-size:18px;cursor:pointer; margin-left:2%; color:red'>").text(""));
		meddiv.append($("<br>"));
		$("#medicine").append(meddiv);
	}


	function addDiag(){
		var dignnodiv = $("<div class ='dignnodiv row'>");
		
		var dignnodiv1 = $("<div class ='col-sm-5'>");
		dignnodiv1.append($("<label  style='font-size:18px;'>").text('Disease :'));
		dignnodiv1.append($("<input required class='mytextbox' name='d_disease[]'>"));
		
		var dignnodiv2 = $("<div class ='col-sm-5'>");
		dignnodiv2.append($("<label style='font-size:18px;'>").text('Comment:'));
		dignnodiv2.append($("<input required class='mytextbox' name='d_comment[]'>"));

		dignnodiv.append(dignnodiv1);
		dignnodiv.append(dignnodiv2);
		dignnodiv.append($("<span class='fa fa-minus myroundminus' onclick='menus(this)' style='font-size:19px;cursor:pointer; color:red'>").text(""));
		dignnodiv.append($("<hr>"));

		$("#diagnose").append(dignnodiv);
	}

	function addSymp(){
		var newdp = $("<div class ='newd row'>");
		
		var newd = $("<div class ='col-sm-5'>");
		newd.append($("<label  style='font-size:18px;'>").text('Title: '));
		newd.append($("<input required class='mytextbox' name='s_title[]'>"));
		
		var newd2 = $("<div class ='col-sm-5'>");
		newd2.append($("<label style='font-size:18px; '>").text('Value: '));
		newd2.append($("<input required class='mytextbox' name='s_value[]'>"));


		newdp.append(newd);
		newdp.append(newd2);
		newdp.append($("<span class='fa fa-minus myroundminus' onclick='menus(this)' style='font-size:19px; color:red;cursor:pointer'>").text(""));
		newdp.append($('<hr>'));
		$("#symp").append(newdp);
	}
		
</script>

<style>
	.mytextbox
	{
		border-bottom: 2px solid green;
		font-size: 17px;
		padding: 1px 5px;
		color: #5f5959;
	}

	.myroundminus
	{
		box-shadow: 0px 0px 23px -6px #000;
		background: white;
		border-radius: 19px;
	    display: inline-block;
	    width: 22px;
	    height: 22px;
	    padding: 3px;

	}

	.myroundminus:hover{
		background: #eee;
	}

</style>

@endsection