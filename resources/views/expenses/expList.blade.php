@extends('layouts.app')
@section('content')
  <div class="container" style="margin-left: 5%; margin-right: 5%; background: white">

    	<h2 style="display: inline;">Expenses List</h2>
                  <div class="pull-right" style="margin-right: 5% ; margin-left: 80%; display: inline;">
                     <a href="{{url('add-expense-form')}}" class="btn btn-primary">Add new Exp</a>
                    </div>
                    <hr>
    <table class="table table-hover">
      <thead style="background: #3aea7f">
          <th>Title</th>
          <th>Amount</th>
          <th>Date</th>
          <th>Delete</th>
      </thead>
      <tbody>
      	@foreach(App\Expenses::all() as $exp)
        <tr>
          <td>{{$exp->title}}</td>
          <td>{{$exp->amount}}</td>
          <td>{{$exp->date}}</td>
          <td><a href="{{route('delete-expense', $exp->id)}}" class="btn btn-danger">Delete</a></td>
        </tr>
        @endforeach
      </tbody>
    </table>
</div>
@endsection()