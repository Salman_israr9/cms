@extends('layouts.app')
@section('content')

<style>
	.sspan{
		color:red;
		font-size: 30px;
	}
</style>
<div class="" style="margin-left: 5%">
			<h2 class="sms-title">Add Expense</h2>
			<hr>
			<form method="post" action="{{route('save-expense')}}">
				{{csrf_field()}}
			<div class="form-group col-md-6">
				<label class="control-lable">Title <span class="sspan">*</span></label>
				<input type="text" name="title" required class="form-control">
			</div>

			<div class="form-group col-md-6">
				<label class="control-lable">Amount <span class="sspan">*</span></label>
				<input type="number" step="any" required name="amount" class="form-control">
			</div>
			

			<div class="form-group col-md-6">
				<label class="control-lable">Date <span class="sspan">*</span></label>
				<input type="date" name="date" required class="form-control">
			</div>


			<div class="form-group col-md-6">
				<label class="control-lable">Other Detail <span class="sspan" style="font-size: 12px">(optional)</span></label>
				
				<textarea style="resize: none;" name="otherDetail" class="form-control" rows="4"></textarea>
			</div>
			
			<div class="form-group col-6">
				<input type="submit" name="submit" value="Submit" class="form-control btn btn-primary">
			</div>
		</form>			
	</div>
	<script type="text/javascript">
		
	</script>
@endsection()